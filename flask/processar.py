import pandas as pd
import requests
import sqlite3
import configparser

CSV_DIR = '/flask/'

config = configparser.ConfigParser()
config.read('/flask/config.ini')
FORMS = config['DEFAULT']['link']

def remover(texto):
    return (texto[texto.find('-')+2:len(texto)])

def link2csv(url,CSV_DIR,nomearquivo):
    #Baixando arquivo
    arquivo = CSV_DIR + nomearquivo
    arquivo_temp = CSV_DIR + 'temp.ods'
    r = requests.get(url, allow_redirects=True)
    open(arquivo_temp, 'wb').write(r.content)

    #Importando em DF
    df = pd.read_excel(arquivo_temp, engine='odf',index_col=0)
    df.to_csv(arquivo,sep=';')
    
def main():
    '''
    Ajustando a planilha baixada do google docs
    '''
    link2csv(FORMS,CSV_DIR,'forms.csv')
    df_forms = pd.read_csv(CSV_DIR + 'forms.csv',sep=';')
    form = df_forms.tail(1)
    form = form.iloc[0]['link']
    form = form.replace("edit?usp=sharing","export?format=ods")
    link2csv(form,CSV_DIR,'input.csv')
    df_hosp = pd.read_csv(CSV_DIR + 'leitos.csv',sep=';')
    cariri = ['HOSPITAL MATERNIDADE AGENOR ARAUJO','HOSPITAL MATERNIDADE SANTO ANTONIO HMSA','HOSPITAL MATERNIDADE SAO VICENTE DE PAULO','HOSPITAL MUNICIPAL DE JARDIM','HOSPITAL REGIONAL DE ICO PREFEITO WALFRIDO MONTEIRO SOBRINHO','HOSPITAL REGIONAL DE IGUATU','HOSPITAL SAO RAIMUNDO','HOSPITAL SAO VICENTE','HOSPITAL SAO VICENTE FERRER','HOSP MUNIC NOSSA SENHORA DAS DORES','HRC - HOSPITAL REGIONAL DO CARIRI','IMTAVI']
    df_hosp['unidadeSaude'] = df_hosp['unidadeSaude'].map(lambda x: remover(str(x)))
    df_hosp = df_hosp[df_hosp['unidadeSaude'].isin(cariri)]
    
    conn = sqlite3.connect(CSV_DIR + 'dados.cariri.hoje.sqlite3')
    #DADOS UTI
    df_uti = df_hosp[df_hosp['tipoLeito']=='UTI']
    agrupados_uti = df_uti.groupby('data').sum()
    agrupados_uti['uti'] = (agrupados_uti['quantidadeLeitosOcupados']/agrupados_uti['quantidadeLeitosAtivos'])*100
    agrupados_uti['uti'] = agrupados_uti['uti'].round(1)
    agrupados_uti.drop(columns=['quantidadeLeitosOcupados','quantidadeLeitosAtivos'],inplace=True)
    agrupados_uti.to_sql("dadosInternacoes2",conn,if_exists='replace',index=True)
    #DADOS DE ENFERMARIA
    df_enfermaria = df_hosp[df_hosp['tipoLeito']=='ENFERMARIA']
    agrupados_enfermaria = df_enfermaria.groupby('data').sum()
    agrupados_enfermaria['enf'] = (agrupados_enfermaria['quantidadeLeitosOcupados']/agrupados_enfermaria['quantidadeLeitosAtivos'])*100
    agrupados_enfermaria['enf'] = agrupados_enfermaria['enf'].round(1)
    agrupados_enfermaria.drop(columns=['quantidadeLeitosOcupados','quantidadeLeitosAtivos'],inplace=True)
    agrupados_enfermaria.to_sql("dadosEnfermaria",conn,if_exists='replace',index=True)
    #print(agrupados_enfermaria.tail(14))
main()

