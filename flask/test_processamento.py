# -*- coding: utf-8 -*-
import pdfplumber
import pandas as pd
import sqlite3
import multiprocessing
import numpy as np
import requests
from bs4 import BeautifulSoup
import re
from unidecode import unidecode
import tabula
import pytest
import os
from pathlib import Path
import PyPDF2

'''
def test_baixar_crato1():
    pagina_crato = requests.get('https://crato.ce.gov.br/publicacoes.php?grupo=37',allow_redirects=True,headers={"User-Agent": "XY"})
    soup = BeautifulSoup(pagina_crato.text, "html.parser")
    hrefs = soup.find_all("a",{"class":"list-group-item"})
    hrefs = hrefs[0:2]
    link = 'https://crato.ce.gov.br/'
    hrefs[0]['href'] = link + hrefs[0]['href']
    hrefs[1]['href'] = link + hrefs[1]['href']
    assert '.pdf' in hrefs[0]['href']
    assert '.pdf' in hrefs[1]['href']
    d1 = requests.get(hrefs[0]['href'])
    d2 = requests.get(hrefs[1]['href'])
    open('crato1_test.pdf','wb').write(d1.content)
    open('crato2_test.pdf','wb').write(d2.content)
    f1 = Path('crato1_test.pdf')
    f2 = Path('crato2_test.pdf')
    assert f1.exists()
    assert f2.exists()
    assert f1.stat().st_size>0
    assert f2.stat().st_size>0
    try:
        PyPDF2.PdfFileReader(open("crato1_test.pdf", "rb"))
    except PyPDF2.utils.PdfReadError:
        raise AssertionError
    try:
        PyPDF2.PdfFileReader(open("crato2_test.pdf", "rb"))
    except PyPDF2.utils.PdfReadError:
        raise AssertionError
'''

def test_crato1():
    pdf = pdfplumber.open('crato1_test.pdf')
    page = pdf.pages[0]
    table = page.extract_table()
    df = pd.DataFrame(table[3:][:])
    df.drop(inplace=True,columns=[1,2,3,4,7])
    assert df is not None
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    os.remove('crato1_test.pdf')
    
def test_crato2():
    pdf = pdfplumber.open('crato2_test.pdf')
    page = pdf.pages[0]
    table = page.extract_table()
    df = pd.DataFrame(table[3:][:])
    df.drop(inplace=True,columns=[1,2,3,4,7])
    assert df is not None
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    os.remove('crato2_test.pdf')
 
def test_jua():
    html_jua = requests.get("https://juazeirodonorte.ce.gov.br/relatorio.php?id=40&rel=",allow_redirects=True,headers={"User-Agent": "XY"})
    open('vacinados_jua_test.html','wb').write(html_jua.content)
    html = ''
    try:
        with open('vacinados_jua_test.html') as f:
            html = f.read()
    except Exception:
        raise AssertionError
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find('tbody')
    row_data = []
    for row in table.find_all("tr"):
        cols = row.find_all("td")
        cols = [ele.text.strip() for ele in cols]
        row_data.append(cols)
    df = pd.DataFrame(row_data)
    df.drop(inplace=True,columns=[1,2,3,4])
    assert df is not None
    df = df[[0,6,5]]
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    os.remove('vacinados_jua_test.html')

def test_barbalha():
    tables = tabula.read_pdf('barbalha1.pdf', pages=[952,953],multiple_tables = True)
    for tabela in tables:
        df = tabela.copy()
        if df.shape[1]==5:
            df.columns = [1,2,3,4,5]
            df = df.tail(df.shape[0]-6)
            df.drop(inplace=True,columns=[1,3,4,5])
        elif df.shape[1]==6:
            df.columns = [1,2,3,4,5,6]
            df = df.tail(df.shape[0]-5)
            df.drop(inplace=True,columns=[1,3,4,5,6])
        else:
            df.columns = [1,2,3,4]
            df = df.tail(df.shape[0]-5)
            df.drop(inplace=True,columns=[1,3,4])
        try:
            df['dose'] = '-'
            df['fabricante'] = '-'
            df.columns = ['nome','dose','fabricante']
            df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
            df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
        except Exception:
            raise AssertionError
        

def test_brejo():
    url = 'https://www.brejosanto.ce.gov.br/vacinometro-oficial/'
    html = requests.get(url)
    soup = BeautifulSoup(html.text, "html.parser")
    table = soup.find('tbody')
    row_data = []
    for row in table.find_all("tr"):
        cols = row.find_all("td")
        cols = [ele.text.strip() for ele in cols]
        row_data.append(cols)
    df = pd.DataFrame(row_data)
    df.drop(inplace=True,columns=[1,2,3,6,7])
    assert df is not None
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
