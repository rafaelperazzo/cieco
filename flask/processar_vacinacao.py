import sqlite3
import pandas as pd


'''

SELECT DISTINCT d.nome as docentes, "DOCENTE" as tipo FROM docentes as d 
LEFT JOIN juazeiro as j on d.nome=j.nome where j.nome is null

UNION

SELECT DISTINCT d.nome as taes, "TAE" as tipo FROM taes as d 
LEFT JOIN juazeiro as j on d.nome=j.nome where j.nome is null

UNION

SELECT DISTINCT d.nome as discentes, "DISCENTE" as tipo FROM discentes as d 
LEFT JOIN juazeiro as j on d.nome=j.nome where j.nome is null ORDER BY tipo,d.nome;

'''

conn = sqlite3.connect('vacinas.sqlite3')
docentes = """
    SELECT count(DISTINCT d.nome) as docentes FROM docentes as d INNER JOIN juazeiro as j on d.nome=j.nome
    """
total_docentes = """
    SELECT count(DISTINCT nome) as total_docentes FROM docentes
    """
discentes = """
    SELECT count(DISTINCT d.nome) as discentes FROM discentes as d INNER JOIN juazeiro as j on d.nome=j.nome
    """
total_discentes = """
    SELECT count(DISTINCT nome) as total_discentes FROM discentes
    """
taes = """
    SELECT count(DISTINCT d.nome) as taes FROM taes as d INNER JOIN juazeiro as j on d.nome=j.nome
    """
total_taes = """
    SELECT count(DISTINCT nome) as total_taes FROM taes
    """
df1 = pd.read_sql(docentes,conn)
df2 = pd.read_sql(total_docentes,conn)

df3 = pd.read_sql(discentes,conn)
df4 = pd.read_sql(total_discentes,conn)

df5 = pd.read_sql(taes,conn)
df6 = pd.read_sql(total_taes,conn)

result = pd.concat([df1,df2,df3,df4,df5,df6],axis=1)
result['percentual_docentes'] = round((result['docentes']/result['total_docentes'])*100,2)
result['percentual_discentes'] = round((result['discentes']/result['total_discentes'])*100,2)
result['percentual_taes'] = round((result['taes']/result['total_taes'])*100,2)
print(result)
result.to_sql("resumo",conn,if_exists='replace',index=False)
conn.close()