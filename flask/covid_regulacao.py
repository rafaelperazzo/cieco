# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from time import sleep
import time
from datetime import date
import logging
from bs4 import BeautifulSoup
import pandas as pd
import sqlite3

#Fonte: Secretaria de Saúde/CE
CSV_DIR = '/share/'
#CSV_DIR = '/home/perazzo/docker/projetos/cieco/'
ARQUIVO = CSV_DIR + 'dados.cariri.hoje.sqlite3'
logging.basicConfig(filename=CSV_DIR + 'covid.internacoes.log', filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
url = 'https://integrasus.saude.ce.gov.br/#/indicadores/indicadores-coronavirus/analise-flexibilizacao?modoExibicao=painel'

def getHTML(headless=False):

    today = date.today()
    data_hoje = today.strftime("%Y-%m-%d")
    #abrindo o navegador
    options = Options()
    options.headless = headless
    driver = webdriver.Firefox(options=options)
    #BAIXANDO DA SECRETARIA DA SAUDE/CE
    driver.get(url)
    sleep(5)
    webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
    sleep(10)
    webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()    
    
    botao = driver.find_element_by_css_selector("span.ng-tns-c117-24")
    botao.click()
    botao = driver.find_element_by_css_selector("#mat-option-190 > span:nth-child(1)")
    botao.click()
    sleep(10)
    html = driver.page_source
    with open(CSV_DIR + 'sec-ce-regulacao-hoje.html', 'w') as arquivo:
        arquivo.write(html)
        arquivo.close()
    #Finalizando
    sleep(10)
    #aguardaDesaparecerCarregando(driver,delay)
    driver.quit()

def ajustar(texto):
    retorno = texto.lstrip()
    retorno = retorno.rstrip()
    retorno = retorno.replace('%','')
    retorno = retorno.replace('.','')
    retorno = retorno.replace(',','.')
    return (retorno)
dia_hoje = int(date.today().strftime("%d"))
#print("Baixando HOJE")
getHTML(headless=False,hoje=True)


html = open(CSV_DIR + 'sec-ce-regulacao-hoje.html','r')
soup = BeautifulSoup(html,'lxml')
valores = soup.find_all("h1")
regulacao = 0
try:
    regulacao = valores[5].string
    regulacao = ajustar(regulacao)
except ValueError:
    logging.error('Erro ao tentar salvar o valor da regulacao!')
    regulacao = 0

today = date.today()
data_hoje = today.strftime("%Y-%m-%d")
df = pd.DataFrame([[data_hoje,regulacao]],columns=["data","regulacao"])
conn = sqlite3.connect(ARQUIVO)
df.to_sql("dadosRegulacao",conn,if_exists='append',index=False)
conn.close()