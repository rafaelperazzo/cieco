import pdfplumber
import pandas as pd
import sqlite3
import multiprocessing
import numpy as np
import requests
from bs4 import BeautifulSoup
import tabula
from unidecode import unidecode

def processar_crato(inicio,fim,pdf):
    conn = sqlite3.connect('vacinas.sqlite3')
    if (inicio==1):
        page = pdf.pages[0]
        table = page.extract_table()
        df = pd.DataFrame(table[3:][:])
        df.drop(inplace=True,columns=[1,2,3,6])
        df.columns = ['nome','dose','fabricante']
        df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
        df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
        df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
        df.to_sql("juazeiro",conn,if_exists='replace',index=False)
        conn.close()
    erros = 0
    paginas = pdf.pages[inicio:fim]
    conn = sqlite3.connect('vacinas.sqlite3')
    for i in range(1,(int)(len(paginas)),1):
        try:
            tabela = paginas[i].extract_table()
            df = pd.DataFrame(tabela[:][:])
            df.drop(inplace=True,columns=[1,2,3,4,7])
            df.columns = ['nome','dose','fabricante']
            df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
            df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
            df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
            df.to_sql("juazeiro",conn,if_exists='append',index=False)
            print('Finalizada pagina ' + str(inicio + i) + ' de ' + str(fim))
        except Exception as e:
            print(str(e))
            erros+=1
            exit()

    print('Erros: ' + str(erros))
    pdf.close()
    conn.close()

def processar_brejo():
    conn = sqlite3.connect('vacinas.sqlite3')
    url = 'https://www.brejosanto.ce.gov.br/vacinometro-oficial/'
    html = requests.get(url)
    soup = BeautifulSoup(html.text, "html.parser")
    table = soup.find('tbody')
    row_data = []
    for row in table.find_all("tr"):
        cols = row.find_all("td")
        cols = [ele.text.strip() for ele in cols]
        row_data.append(cols)
    df = pd.DataFrame(row_data)
    df.drop(inplace=True,columns=[1,2,3,6,7])
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
    df.to_sql("juazeiro",conn,if_exists='append',index=False)
    conn.close()

def processar_barbalha(inicio,fim,total):
    conn = sqlite3.connect('vacinas.sqlite3')
    tables = tabula.read_pdf('barbalha1.pdf', pages=list(range(inicio,fim)),multiple_tables = True)
    i = inicio
    for tabela in tables:
        df = tabela.copy()
        if df.shape[1]==5:
            df.columns = [1,2,3,4,5]
            df = df.tail(df.shape[0]-6)
            df.drop(inplace=True,columns=[1,3,4,5])
        elif df.shape[1]==6:
            df.columns = [1,2,3,4,5,6]
            df = df.tail(df.shape[0]-5)
            df.drop(inplace=True,columns=[1,3,4,5,6])
        else:
            df.columns = [1,2,3,4]
            df = df.tail(df.shape[0]-5)
            df.drop(inplace=True,columns=[1,3,4])
        try:
            df['dose'] = '-'
            df['fabricante'] = '-'
            df.columns = ['nome','dose','fabricante']
            df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
            df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
            df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
            df.to_sql("juazeiro",conn,if_exists='append',index=False)
            print("Finalizada pagina " + str(i) + " de " + str(total))
        except Exception as e:
            print(str(e))
            exit()
        i = i + 1
    conn.close()

def processar_juazeiro():
    html_jua = requests.get("https://juazeirodonorte.ce.gov.br/relatorio.php?id=40&rel=",allow_redirects=True,headers={"User-Agent": "XY"})
    open('vacinados_jua.html','wb').write(html_jua.content)
    conn = sqlite3.connect('vacinas.sqlite3')
    html = ''
    try:
        with open('vacinados_jua.html') as f:
            html = f.read()
    except Exception:
        print('ERRO AO PROCESSAR JUAZEIRO')
        return
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find('tbody')
    row_data = []
    for row in table.find_all("tr"):
        cols = row.find_all("td")
        cols = [ele.text.strip() for ele in cols]
        row_data.append(cols)
    df = pd.DataFrame(row_data)
    df.drop(inplace=True,columns=[1,2,3,4])
    df = df[[0,6,5]]
    df.columns = ['nome','dose','fabricante']
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
    df.to_sql("juazeiro",conn,if_exists='append',index=False)
    conn.close()

def importar_comunidade():
    conn = sqlite3.connect('vacinas.sqlite3')
    df = pd.read_csv('docentes.csv',sep=';')
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
    df.to_sql("docentes",conn,if_exists='replace',index=False)
    df = pd.read_csv('discentes.csv',sep=';')
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
    df.to_sql("discentes",conn,if_exists='replace',index=False)
    df = pd.read_csv('taes.csv',sep=';')
    df['nome'] = df['nome'].astype(str).map(lambda x: unidecode(x))
    df['nome'] = df['nome'].astype(str).map(lambda x: x.upper())
    df['nome'] = df['nome'].astype(str).map(lambda x: x.strip())
    df.to_sql("taes",conn,if_exists='replace',index=False)
    conn.close()

if __name__ == "__main__":
    '''
    COMUNIDADE ACADEMICA
    '''
    print('IMPORTANDO COMUNIDADE ACADEMICA')
    try:
        importar_comunidade()
    except Exception as e:
        print(str(e))
        exit()

       
    

    
    '''
    CRATO
    '''
    try:
        '''
        print('PROCESSANDO CRATO - Baixando arquivos...')
        pagina_crato = requests.get('https://crato.ce.gov.br/publicacoes.php?grupo=37',allow_redirects=True,headers={"User-Agent": "XY"})
        soup = BeautifulSoup(pagina_crato.text, "html.parser")
        hrefs = soup.find_all("a",{"class":"list-group-item"})
        hrefs = hrefs[0:2]
        link = 'https://crato.ce.gov.br/'
        hrefs[0]['href'] = link + hrefs[0]['href']
        hrefs[1]['href'] = link + hrefs[1]['href']
        d1 = requests.get(hrefs[0]['href'])
        d2 = requests.get(hrefs[1]['href'])
        open('crato1.pdf','wb').write(d1.content)
        open('crato2.pdf','wb').write(d2.content)
        '''
        print('PROCESSANDO CRATO - 1a Dose')
        pdf = pdfplumber.open('crato1.pdf')
        total = len(pdf.pages)
        paginas = list(range(0,total,1))
        splits = np.array_split(paginas,50)
        for subconjunto in splits:
            atual = list(subconjunto)
            p = multiprocessing.Process(target=processar_crato,args=(atual[0],atual[-1]+1,pdf),name='p')
            p.start()
            p.join()
        pdf.close()

        print('PROCESSANDO CRATO - 2a Dose')
        pdf = pdfplumber.open('crato2.pdf')
        total = len(pdf.pages)
        paginas = list(range(0,total,1))
        splits = np.array_split(paginas,50)
        for subconjunto in splits:
            atual = list(subconjunto)
            p = multiprocessing.Process(target=processar_crato,args=(atual[0],atual[-1]+1,pdf),name='p')
            p.start()
            p.join()
        pdf.close()
    except Exception as e:
        print(str(e))
        exit()
    
    '''
    BARBALHA
    '''
    
    print('PROCESSANDO BARBALHA')
    try:
        pdf = pdfplumber.open('barbalha1.pdf')
        total = len(pdf.pages)
        pdf.close()
        paginas = list(range(1,total,1))
        splits = np.array_split(paginas,50)
        for subconjunto in splits:
            atual = list(subconjunto)
            p = multiprocessing.Process(target=processar_barbalha,args=(atual[0],atual[-1]+1,total),name='p')
            p.start()
            p.join()
    except Exception as e:
        print(str(e))
        exit()


    '''
    JUAZEIRO DO NORTE
    '''
    print('PROCESSANDO JUAZEIRO DO NORTE')
    try:
        processar_juazeiro()
    except Exception as e:
        print(str(e))
        exit()
    
    '''
    BREJO SANTO
    '''
    print('PROCESSANDO BREJO SANTO')
    try:
        processar_brejo()
    except Exception as e:
        print(str(e))
        exit()