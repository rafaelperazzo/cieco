cd /dados/apps/cieco/
wget -O flask/leitos.zip https://download-integrasus.saude.ce.gov.br/leitos_ativos_ocupados_por_unidade_saude
cd flask
unzip leitos.zip
mv leitos_ativos_ocupados_por_unidade_saude.csv leitos.csv
rm leitos.zip
cp /dados/apps/share/dados.cariri.hoje.sqlite3 /dados/apps/cieco/flask/
docker-compose exec cieco python /flask/processar.py
#docker-compose exec cieco python /flask/processar_pdf.py
docker-compose down
docker-compose up -d
