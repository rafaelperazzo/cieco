from flask import Flask,render_template
from waitress import serve
import pandas as pd
import sqlite3
import math
import logging
import json
from git import Repo

CSV_DIR = '/flask/'

app = Flask(__name__)

logging.basicConfig(filename=CSV_DIR + 'app.log', filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)

repo = Repo('/git')
tagref = str(repo.tags[len(repo.tags)-1])
app.config['APP_VERSION'] = tagref

def tendencia_descrescente(linhas,coluna):
    if (len(linhas)<2):
        return (True,0)
    soma_x = 0
    soma_y = 0
    soma_xy = 0
    soma_xx = 0
    i = 1
    for linha in linhas:
        soma_x = soma_x + i
        soma_y = soma_y + float(linha[coluna])
        soma_xy = soma_xy + (float(linha[coluna])*i)
        soma_xx = soma_xx + (i*i)
        i = i + 1
    n = len(linhas)
    a1 = ((n*soma_xy)-(soma_x*soma_y))/((n*soma_xx)-(soma_x*soma_x))
    a0 = ((soma_xx*soma_y)-(soma_xy*soma_x))/(n*soma_xx-soma_x*soma_x)
    angulo = (a1*2+a0)-(a1*1+a0)
    angulo = math.degrees(math.atan(angulo))
    if a1<=0:
        return(True,angulo)
    else:
        return (False,angulo)

def tendencia(linhas):
    soma_x = 0
    soma_y = 0
    soma_xy = 0
    soma_xx = 0
    i = 1
    for linha in linhas:
        soma_x = soma_x + i
        soma_y = soma_y + float(linha)
        soma_xy = soma_xy + (float(linha)*i)
        soma_xx = soma_xx + (i*i)
        i = i + 1
    n = len(linhas)
    a1 = ((n*soma_xy)-(soma_x*soma_y))/((n*soma_xx)-(soma_x*soma_x))
    a0 = ((soma_xx*soma_y)-(soma_xy*soma_x))/(n*soma_xx-soma_x*soma_x)
    
    return(a1,a0)

def processar():
    '''
    Ajustando a planilha baixada do google docs
    '''
    df_forms = pd.read_csv(CSV_DIR + 'forms.csv',sep=';')
    form = df_forms.tail(1)
    form = form.iloc[0]['link']
    form = form.replace("edit?usp=sharing","export?format=ods")
    data = df_forms.tail(1).iloc[0]['data']
    '''
    DIMENSÃO 3
    '''
    d3 = df_forms.tail(1).iloc[0]['dimensao3']
    df_setores = pd.read_csv(CSV_DIR + 'setores.csv',sep=",")
    df_setores['setor'] = df_setores['setor'].str.upper()
    df = pd.read_csv(CSV_DIR + 'input.csv',sep=';')
    frames = [df.iloc[:,2],df.iloc[:,4],df.iloc[:,5],df.iloc[:,6],df.iloc[:,12],df.iloc[:,15]]
    result = pd.concat(frames,axis=1)
    result.columns = ["setor","1","2","3","4","5"]
    result.drop_duplicates(subset=['setor'],keep='last',inplace=True)
    result.fillna(0,inplace=True)
    result["setor"] = result["setor"].str.upper()
    result.replace(to_replace="Não ocorreu",value=4,inplace=True)
    result.replace(to_replace="Ocorreu parcialmente",value=3,inplace=True)
    result.replace(to_replace="Ocorreu totalmente",value=2,inplace=True)
    result.replace(to_replace="Supera",value=1,inplace=True)
    result.replace(to_replace="Não se aplica, pois atividades presenciais NÃO ocorreram em nenhum local de meu setor",value=0,inplace=True)
    result.replace(to_replace="Não se aplica, pois atividades presenciais não ocorreram em nenhum local de meu setor",value=0,inplace=True)
    result.replace(to_replace="Sim",value=2,inplace=True)
    result.replace(to_replace="Não",value=1,inplace=True)
    '''
    Inserindo a coluna para os agrupamentos
    '''
    result = result.merge(df_setores,how='left',on='setor')
    result.drop(["id"],axis=1,inplace=True)
    result['score'] = (result['1'] + result['2'] + result['3'] + result['4'] + result['5'])/5
    #result['score'] = result['score']*result['peso']
    #print(result.sort_values(by=['peso']))
    result.to_csv(CSV_DIR + 'output.csv',sep=';')
    '''
    DIMENSÃO 4
    '''
    totais = result.groupby('peso').mean()
    d4 = totais['score'].to_list()
        
    '''
    SQLITE - INFORMAÇÕES DA MÉDIA MÓVEL
    '''
    
    conn = sqlite3.connect(CSV_DIR + 'dados.cariri.hoje.sqlite3')
    cursor = conn.cursor()
    consulta = """
    select * from evolucaoMedia WHERE (data between date('now','-14 day') 
    AND date('now')) AND cidade='TODAS AS CIDADES' ORDER BY data ASC
    """
    cursor.execute(consulta)
    linhas = cursor.fetchall()
    '''
    DIMENSÃO 2
    '''
    d2_2,d2_2_a1 = tendencia_descrescente(linhas,2)
    d2_1,d2_1_a1 = tendencia_descrescente(linhas,3)

    d2_valor = linhas
    '''
    DIMENSÃO 1
    '''
    consulta = """
    select * from dadosInternacoes2 order by data desc LIMIT 1
    """
    cursor.execute(consulta)
    linha = cursor.fetchone()
    disponivel = 100-float(linha[1])
    if disponivel>30:
        d1_1 = True
    else:
        d1_1 = False
    d1_1_valor = disponivel

    consulta = """
    select * from dadosInternacoes2 WHERE (data between date('now','-14 day') AND date('now')) 
    order by data ASC
    """
    cursor.execute(consulta)
    linhas = cursor.fetchall()
    d1_2,d1_2_a1 = tendencia_descrescente(linhas,1)
    d1_2_valor = linhas
    consulta = """
    select * from dadosEnfermaria WHERE (data between date('now','-14 day') AND date('now')) 
    order by data ASC
    """
    cursor.execute(consulta)
    linhas = cursor.fetchall()
    d1_3,d1_3_a1 = tendencia_descrescente(linhas,1)
    d1_3_valor = linhas
    consulta = """
    select * from dadosRegulacao order by data ASC LIMIT 14
    """
    cursor.execute(consulta)
    linhas = cursor.fetchall()
    
    d1_4,d1_4_a1 = tendencia_descrescente(linhas,1)
    d1_4_valor = linhas
    dic = {True: "SIM",False:"NAO"}

    #return (data,dic[d1_1],dic[d1_2],dic[d1_3],dic[d1_4],dic[d2_1],dic[d2_2],d3,d4)
    return([data,dic[d1_1],dic[d1_2],dic[d1_3],dic[d1_4],dic[d2_1],dic[d2_2],d3,d4],[round(d1_1_valor,2),d1_2_valor,d1_3_valor,d1_4_valor,d2_valor],[d1_2_a1,d1_3_a1,d1_4_a1,d2_1_a1,d2_2_a1])

def get_valores(tupla,coluna):
    resultado = []
    for linha in tupla:
        resultado.append(linha[coluna])
    return(resultado)

def lista_tendencia(a1,a0,tamanho):
    tendencias = []
    for i in range(0,tamanho,1):
        tendencias.append(a1*i+a0)
    return(tendencias)

def verificar_vacinacao():
    conn = sqlite3.connect('vacinas.sqlite3')
    cursor = conn.cursor()
    p1 = int(cursor.execute("SELECT percentual_docentes FROM resumo").fetchone()[0])
    p2 = int(cursor.execute("SELECT percentual_discentes FROM resumo").fetchone()[0])
    p3 = int(cursor.execute("SELECT percentual_taes FROM resumo").fetchone()[0])
    conn.close()
    return(p1,p2,p3)

@app.route("/")
def root():
    conn = sqlite3.connect(CSV_DIR + 'dados.cariri.hoje.sqlite3')
    cursor = conn.cursor()
    consulta = "UPDATE dadosInternacoes2 SET uti=0 WHERE uti is null"
    cursor.execute(consulta)
    respostas,valores,a1s = processar()
    d1_2_datas = get_valores(valores[1],0)
    d1_2_valores = get_valores(valores[1],1)
    d1_3_datas = get_valores(valores[2],0)
    d1_3_valores = get_valores(valores[2],1)
    d1_4_datas = get_valores(valores[3],0)
    d1_4_valores = get_valores(valores[3],1)
    d2_datas = get_valores(valores[4],0)
    d2_1_valores = get_valores(valores[4],3)
    d2_2_valores = get_valores(valores[4],2)
    graficos = [json.dumps(d1_2_datas),d1_2_valores,d1_3_datas,d1_3_valores,d1_4_datas,d1_4_valores,d2_datas,d2_1_valores,d2_2_valores]
    p1,p2,p3 = verificar_vacinacao()
    pesos = [4,3,3,1,2,2,2,3,1,2,3,4]
    soma = 0
    for i in range(1,8):
        if respostas[i]=='SIM':
            soma = soma + pesos[i-1]
    for i in range(0,len(respostas[8]),1):
        if respostas[8][i]<=2:
            soma = soma + pesos[i+8]
    if p1>=70:
        soma = soma + 4
    if p2>=70:
        soma = soma + 4
    if p3>=70:
        soma = soma + 4
    a1_uti,a0_uti = tendencia(graficos[1])
    tendencia_uti = lista_tendencia(a1_uti,a0_uti,len(graficos[1]))
    a1_enfermaria,a0_enfermaria = tendencia(graficos[3])
    tendencia_enfermaria = lista_tendencia(a1_enfermaria,a0_enfermaria,len(graficos[3]))
    a1_confirmacao,a0_confirmacao = tendencia(graficos[7])
    a1_obitos,a0_obitos = tendencia(graficos[8])
    tendencia_confirmacao = lista_tendencia(a1_confirmacao,a0_confirmacao,len(graficos[7]))
    tendencia_obitos = lista_tendencia(a1_obitos,a0_obitos,len(graficos[8]))
    return (render_template('resultados.html',respostas=respostas,valores=valores,a1s=a1s,soma=soma,graficos=graficos,tendencia_uti=tendencia_uti,tendencia_enfermaria=tendencia_enfermaria,tendencia_confirmacao=tendencia_confirmacao,tendencia_obitos=tendencia_obitos,versao=app.config['APP_VERSION'],p1=p1,p2=p2,p3=p3))

if __name__ == "__main__":
    serve(app, host='0.0.0.0', port=80, url_prefix='/cieco')
