FROM python:3
WORKDIR /flask
RUN pip install --upgrade pip
RUN pip install numpy scipy pandas sympy matplotlib werkzeug Flask lxml unidecode requests pdfkit waitress bs4
RUN pip install odfpy
RUN pip install pyTelegramBotAPI
RUN pip install GitPython pytest
RUN pip install tabula-py unidecode
RUN pip install pdfplumber tabula-py
RUN apt-get update
RUN apt-get -y install default-jre
RUN pip install PyPDF2
EXPOSE 80
CMD python /flask/app.py
