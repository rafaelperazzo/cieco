# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from time import sleep
import time
from datetime import date
import logging
from bs4 import BeautifulSoup
import pandas as pd
import sqlite3

#Fonte: Secretaria de Saúde/CE
CSV_DIR = '/share/'
#CSV_DIR = '/home/perazzo/docker/projetos/cieco/'
ARQUIVO = CSV_DIR + 'dados.cariri.hoje.sqlite3'
logging.basicConfig(filename=CSV_DIR + 'covid.internacoes.log', filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)
url = 'https://integrasus.saude.ce.gov.br/#/indicadores/indicadores-coronavirus/historico-internacoes-covid?modoExibicao=painel'

def getHTML(headless=False):

    today = date.today()
    #abrindo o navegador
    options = Options()
    options.headless = headless
    driver = webdriver.Firefox(options=options)
    #BAIXANDO DA SECRETARIA DA SAUDE/CE
    driver.get(url)
    sleep(40)

    webdriver.ActionChains(driver).send_keys(Keys.ESCAPE).perform()
    botao = driver.find_element_by_css_selector("span.ng-tns-c117-27")
    botao.click()
    sleep(2)
    botao = driver.find_element_by_css_selector("#mat-option-186 > span:nth-child(1)")
    botao.click()
    sleep(20)
    html = driver.page_source
    with open(CSV_DIR + 'sec-ce-internacoes-hoje.html', 'w') as arquivo:
        arquivo.write(html)
    sleep(3)
    driver.quit()
    
dia_hoje = int(date.today().strftime("%d"))
print("Baixando HOJE")
getHTML(headless=True)

html = open(CSV_DIR + 'sec-ce-internacoes-hoje.html','r')
soup = BeautifulSoup(html,'lxml')
valores = soup.find_all('h1')

uti = 0
enfermaria = 0
try:
    uti = valores[0].string
    uti = uti.lstrip()
    uti = uti.rstrip()
    uti = uti.replace("%","")
    enfermaria = valores[4].string
    enfermaria = enfermaria.lstrip()
    enfermaria = enfermaria.rstrip()
    enfermaria = enfermaria.replace("%","")
except ValueError:
    logging.error('Erro ao tentar salvar o valor da ocupação de UTI!')
    uti = 0

today = date.today()
data_hoje = today.strftime("%Y-%m-%d")
df = pd.DataFrame([[data_hoje,uti]],columns=["data","uti"])
df2 = pd.DataFrame([[data_hoje,enfermaria]],columns=["data","enf"])
conn = sqlite3.connect(ARQUIVO)
df.to_sql("dadosInternacoes2",conn,if_exists='append',index=False)
df2.to_sql("dadosEnfermaria",conn,if_exists='append',index=False)
conn.close()
